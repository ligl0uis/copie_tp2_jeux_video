using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutomaticSpriteFlip
{
    Transform transform;
    float scaleMultiplier;
    Vector3 scale;

    // Constructeur (Assignation des variables)
    public AutomaticSpriteFlip(Transform _transform)
    {
        transform = _transform;

        scaleMultiplier = _transform.localScale.x;
        scale = _transform.localScale;
    }

    // Modifie la rotation de l'objet
    public void SetDirection(Direction direction)
    {
        int value = 0;

        switch (direction)
        {
            case Direction.gauche:
                value = -1;
                break;
            case Direction.droite:
                value = 1;
                break;
            default:
                break;
        }

        scale.x = scaleMultiplier * value;
        transform.localScale = scale;
    }

    public enum Direction
    {
        gauche,
        droite
    }
}
