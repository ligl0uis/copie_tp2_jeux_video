using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UIElements;

public class IndicateurDegats : MonoBehaviour
{
    TextMeshPro textMesh;

    Vector3 vecteur;
    GameObject indicateur;
    public void Setup(float degats,Transform position,GameObject prefab)
    {
 
        indicateur = Instantiate(prefab, position.position,Quaternion.identity);
        textMesh = indicateur.GetComponent<TextMeshPro>();

        // Convertir en int pour �viter les d�cimales
        textMesh.text = Mathf.RoundToInt(degats).ToString();

        // Direction de l'animation
        vecteur.x = Random.Range(-1f, 1f);
        vecteur.y = 1f;
        vecteur.z = 0f;

        // Destruction apr�s un d�lai
        Destroy(indicateur, 0.5f);
    }

    void Update()
    {
        if (indicateur != null)
        {
            indicateur.transform.Translate(vecteur * 3f * Time.deltaTime);
        }
    }
}
