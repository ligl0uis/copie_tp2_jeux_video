using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bombe : MonoBehaviour
{
    public float force;
    public float rayon;

    void Start()
    {
        Invoke("Exploser", 1f);
    }

    void Exploser()
    {
        // Récupérer tous les objets à proximité
        Collider2D[] objets = Physics2D.OverlapCircleAll(transform.position, rayon);

        // Vérifier chacun des objets pour un Rigidbody
        foreach (var obj in objets)
        {
            // Tenter de récupérer le Rigidbody
            if (obj.TryGetComponent(out Rigidbody2D rb))
            {
                Vector3 direction = rb.transform.position - transform.position;

                // Appliquer la force
                rb.AddForce(direction * force, ForceMode2D.Impulse);
            }
        }
    }
}
