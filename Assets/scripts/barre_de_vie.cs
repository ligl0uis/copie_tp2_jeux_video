using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UIElements;
public class barre_de_vie : MonoBehaviour
{
    private UnityEngine.UI.Slider slider;

    protected virtual void Start()
    {
        slider = GetComponent<UnityEngine.UI.Slider>();
    }

    public virtual void damage_barre(int vie)
    {
        slider.value = vie;
    }
}
