using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LancerProjectile : MonoBehaviour
{
    public GameObject prefabProjectile;

    Camera mainCamera;

    // Start is called before the first frame update
    void Start()
    {
        mainCamera = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        // Quand j'appuie sur clic-gauche
        if (Input.GetMouseButtonDown(1))
        {
            // Créer une copie du projectile
            GameObject projectile = Instantiate(prefabProjectile, transform.position, Quaternion.identity);

            // Position du curseur
            Vector3 mousePosition = mainCamera.ScreenToWorldPoint(Input.mousePosition);

            // Direction du lancer
            Vector3 direction = mousePosition - transform.position;

            // Rigidbody du projectile
            Rigidbody2D rb = projectile.GetComponent<Rigidbody2D>();

            // Lui donner une force!
            rb.AddForce(direction * 1.5f, ForceMode2D.Impulse);

            // Détruire le projectile après un délai
            Destroy(projectile, 4f);
        }
    }
}
