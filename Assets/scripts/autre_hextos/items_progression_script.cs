﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class items_progression_script : MonoBehaviour
{
    [SerializeField] string object_bloquant;
    GameObject object_bloquant_obj;
    private void Start()
    {
        object_bloquant_obj = GameObject.Find(object_bloquant);
        Debug.Log(object_bloquant_obj.name);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.TryGetComponent<Joueur>(out _))
        {
            Debug.Log("joueur entrée avec la clé");
            Destroy(object_bloquant_obj);
            Destroy(this.gameObject);
        }
        else
        {
            Debug.Log("non joueur entrée avec la clé");
        }
    }
}
