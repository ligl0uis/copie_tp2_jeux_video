using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UIElements;

public class script_projectile_explosif : MonoBehaviour
{
    //float gravite = 9.8f;
    //float hauteurmax = 5f;
    float tempsavantexplosion = 2f;
    float tempsdevol;
    Animator animator;
    CircleCollider2D _collider2D;
    List<Personnage> liste_de_victimes;
    //le temps d'explosion est obtenu a partir du temps statique de l'animation (je pourrait le faire dynamiquement je croit mais sa serait long)
    int damage;

    void Start()
    {
        liste_de_victimes = new List<Personnage>();
        animator = GetComponent<Animator>();
        _collider2D = GetComponent<CircleCollider2D>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    IEnumerator main_trajectoire(Vector2 _deplacement)
    {
        float elapsed_time = 0f;
        while(elapsed_time<tempsdevol)
        {
            float delta = Time.deltaTime;
            Vector2 _deplacement_calc = (_deplacement * delta);
            transform.position += new Vector3(_deplacement_calc.x, _deplacement_calc.y);
            elapsed_time += delta;
            yield return null;
        }
        while(elapsed_time < tempsavantexplosion)
        {
            elapsed_time += Time.deltaTime;
            yield return null;
        }
        explode();
    }
    //la raison pourquoi je fait un main plutot qu'utiliser start c'est pour permettre au meme prefab projectile de potentiellement avoir plusieurs comportements!
    //la classe n'a quand même aucun héritage () car aucun autre type de projectile ce trouve dans le jeu et le temps est court...
    public void main(Vector2 cible,int degat, float basevitesse, float coefficientdistancevitesse, float distanceMax)
    {
        Vector2 position = this.transform.position;
        // Distance entre le projectile et son point d'atterisage
        float distance = Vector2.Distance(position, cible);

        // Si la cible est plus loin que la distance maximale de tir, on corrigera la position de la cible
        if (distance > distanceMax)
        {
            Vector2 direction = (cible - position).normalized;

            // Cible corrigée
            cible = position + direction * distanceMax;
            distance = distanceMax;
            Debug.Log("cible corrigé" + cible.y.ToString() + cible.x.ToString());
        }
        else
        {
            Debug.Log("cible normale" + cible.y.ToString() + cible.x.ToString());
        }
  


        damage = degat;

        Vector2 positionprojectile = transform.position;
        Debug.Log("position projectile : "+positionprojectile.x.ToString() + " " + positionprojectile.y.ToString());

        Vector2 test = cible - positionprojectile;
        Debug.Log("soustraction : "+test.x.ToString() + " " + test.y.ToString());

        float vitesse_vecteur = (basevitesse+(coefficientdistancevitesse*(cible-positionprojectile).magnitude));
        Debug.Log("vecteur vitesse : "+vitesse_vecteur.ToString());

        //cette ligne expose a quel point ma mathématique est mal construite
        tempsdevol = distance/vitesse_vecteur;
        Debug.Log("temps de vol : " + tempsdevol.ToString());

        Vector2 deplacement = ((cible - positionprojectile).normalized)*vitesse_vecteur;
        Debug.Log("deplacement : "+deplacement.x.ToString()+" "+deplacement.y.ToString());

        //le vecteur de déplacement doit etre appliqué a chaque frame multiplié par le nombre de secondes écoulés (0.001 genre)
        StartCoroutine(main_trajectoire(deplacement));
        
        

        //float hauteur = Mathf.Min(hauteurmax, Mathf.Abs(cible.y - positionprojectile.y));
        //float vertical


        //float distance = Vector2.Distance(positionprojectile, cible);
        //float temps_de_vol = distance / vitesse;
        //-b = -(gravite/2)*temps_de_vol
        //float b = (gravite / 2) * temps_de_vol;



        //float hauteur = Mathf.Min(hauteurMaximale, Mathf.Abs(positionCible.y - positionDepart.y));

        //float vitesseVerticale = Mathf.Sqrt(2f * Mathf.Abs(Physics2D.gravity.y) * hauteur);

        //float vitesseHorizontale = direction.magnitude / (hauteur / vitesseVerticale + (vitesseVerticale / Mathf.Abs(Physics2D.gravity.y)));
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        Personnage victime_potentielle;
        Debug.Log(collision.gameObject.name);
        try
        {
            victime_potentielle = collision.gameObject.GetComponent<Personnage>();
        }
        catch
        {
            return;
        }
        foreach(Personnage victime in liste_de_victimes)
        {
            if(victime==victime_potentielle)
            {
                return;
            }
        }
        //maintenant on victimise (on pourrait faire des propulsions mais c'est compliqué pour rien et je croit que les goblins n'ont pas de rigid body)
        victime_potentielle.subirDegats(damage);
        //la victime potentielle devient une victime et ne peut plus être victimisé :))
        liste_de_victimes.Add(victime_potentielle);
    }

    //la destruction de l'objet a lieu dans l'animator
    void explode()
    {
        GetComponent<AudioSource>().Play();
        animator.SetTrigger("explode");
        _collider2D.enabled = true;
    }
}
