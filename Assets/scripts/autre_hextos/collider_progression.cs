using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class collider_progression : MonoBehaviour
{
    [SerializeField] AudioClip[] clips1;
    [SerializeField] AudioClip[] clips2;
    [Header("bas vers haut, droite vers gauche")]
    [SerializeField] int[] ids;
    [SerializeField] GameManager gameManager;
    [SerializeField] bool verif_sur_x;
    
    void Start()
    {
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if(collision.gameObject.TryGetComponent<Joueur>(out _))
        {
            Vector3 joueur = collision.transform.position;
            if(verif_sur_x)
            {
                if(joueur.x<gameObject.transform.position.x)
                {
                    gameManager.changer_zone(ids[0], clips1[Random.Range(0, clips1.Length)]);
                }
                else
                {
                    gameManager.changer_zone(ids[1], clips2[Random.Range(0, clips2.Length)]);
                }
                    
            }
            else
            {
                if (joueur.y < gameObject.transform.position.y)
                {
                    gameManager.changer_zone(ids[0], clips1[Random.Range(0, clips1.Length)]);
                }
                else
                {
                    gameManager.changer_zone(ids[1], clips2[Random.Range(0, clips2.Length)]);
                }
            }
        }
        
    }
}
