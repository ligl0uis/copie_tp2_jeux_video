﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cave_layers_script : MonoBehaviour
{
    //car le joueur commence au dessus du sol
    bool souslesol = false;
    [SerializeField] GameObject audessus_empty;

    //aucune verifications car j'ai pas le temps... je fait la vallée de biston a la rache et j'ai "finit"
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(souslesol)
        {
            audessus_empty.SetActive(true);
            souslesol=false;
        }
        else
        {
            audessus_empty.SetActive(false);
            souslesol = true;
        }
    }
}
