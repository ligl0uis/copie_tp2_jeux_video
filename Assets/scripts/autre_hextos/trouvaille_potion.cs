using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class trouvaille_potion : MonoBehaviour
{
    Collider2D coliider;
    Personnage perso;
    // Start is called before the first frame update
    void Start()
    {
        coliider = GetComponent<Collider2D>();
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.TryGetComponent<Personnage>(out perso))
        {
            perso.soigner(20f);
            Destroy(this.gameObject);
        }
    }
}
