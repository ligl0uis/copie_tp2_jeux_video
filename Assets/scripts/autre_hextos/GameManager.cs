﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    //insatisfaisant...
    List<GameObject> village_goblins;
    List<GameObject> valle_goblins;
    List<GameObject> cave_goblins;
    bool village_check = false;
    bool valle_check = false;
    AudioSource source;
    [SerializeField] GameObject village;
    [SerializeField] GameObject valle;
    [SerializeField] GameObject cave;
    [SerializeField] GameObject clé_valle;
    [SerializeField] GameObject clé_cave;
    [SerializeField] GameObject gameover;
    [SerializeField ]private TextMeshProUGUI temps_affichage;
    [SerializeField] private TextMeshProUGUI zone_affichage;
    string[] zones;
    private float tempsecoule = 0;

    private void Update()
    {
        tempsecoule += Time.deltaTime;
        TimeSpan span = TimeSpan.FromSeconds(tempsecoule);
        //Debug.Log(temps_affichage.text);
        temps_affichage.text = span.Hours.ToString()+":"+span.Minutes.ToString()+":"+span.Seconds.ToString();
    }
    void Start()
    {
        zones = new string[3] { "Hextos", "vallee de Biston","grottes de Merti" };
        // Assigner l'audio source
        source = GetComponent<AudioSource>();
        source.Play(0);

        Invoke("delay_obtain", 5f);

    }
    void obtain_children(GameObject empty, List<GameObject> liste)
    {
        foreach (Transform goblins in empty.transform)
        {
            liste.Add(goblins.gameObject);
        }
        foreach (GameObject goblin in liste)
        {
            Debug.Log(goblin.name);
        }
        Debug.Log(liste.Count.ToString());
    }
    public void ennemy_check(Vector2 position)
    {
        if (!village_check)
        {
            StartCoroutine(delay_remove(village_goblins));
            //schrodinger's destruction de main thread! si la coroutine est incapable de modifier barre la function sera eternelle!
            //while(barre)
            //{
                //je confirme que sa oblitere le main thread...
                //au moins des fois on s'amuse en programmant...
            //}
            //barre = true;
            Debug.Log("MONTANT RESTANT DANS LE VILLAGE!!:" + village_goblins.Count.ToString());
            foreach (GameObject goblin in village_goblins)
            {
                Debug.Log($"{goblin.name}");
            }
            StartCoroutine(village_event(new Vector3(position.x, position.y)));
            
        }
        else
        {
            if (!valle_check)
            {
                StartCoroutine(delay_remove(valle_goblins));
                StartCoroutine(valle_event(new Vector3(position.x, position.y)));
            }
            else
            {
                StartCoroutine(delay_remove(cave_goblins));
                StartCoroutine(cave_event(new Vector3(position.x, position.y)));
            }
        }
    }
    IEnumerator village_event(Vector3 position)
    {
        yield return new WaitForSeconds(2.5f);
        if (village_goblins.Count == 0)
        {

            Instantiate(clé_valle,position,Quaternion.identity);
            village_check =true;
        }
        yield break;
    }
    IEnumerator valle_event(Vector3 position)
    {

        yield return new WaitForSeconds(2.5f);
        if (valle_goblins.Count == 0)
        {
            Instantiate(clé_cave, position, Quaternion.identity);
            valle_check = true;
        }
        yield break;
    }
    IEnumerator cave_event(Vector3 position)
    {
        yield return new WaitForSeconds(2.5f);
        if (cave_goblins.Count == 0)
        {
            Instantiate(gameover, position, Quaternion.identity);
        }
        yield break;
    }
    void changer_musique(AudioClip clip)
    {
        source.Stop();
        source.clip=clip;
        source.Play(0);
    }
    void delay_obtain()
    {
        //les delai et pour eviter que cette partie du start se fasse avant les start des goblins resultant en nullreference. Je suis certain qu'il y a un meilleur facon genre les goblins ce rapportent au gamemanager lorsqu'il sont créé mais le temps est court
        //pourrait faire des for mais il y a trois zones
        village_goblins= new List<GameObject>();
        valle_goblins= new List<GameObject>();
        cave_goblins= new List<GameObject>();

        obtain_children(village, village_goblins);
        obtain_children(valle, valle_goblins);
        obtain_children(cave, cave_goblins);
    }
    IEnumerator delay_remove(List<GameObject> _list)
    {
        yield return new WaitForSeconds(2f);
        _list.RemoveAll(x => x == null);
        //barre=false;
        yield break;
    }

    //les id sont en ordre de progression
    public void changer_zone(int id_zone,AudioClip audio = null)
    {
        if (audio != null)
        {
            changer_musique(audio);
        }
        Debug.Log("CHANGER ZONE ID"+id_zone.ToString() + zones[id_zone]+ zones.ToString()); 
        zone_affichage.text = zones[id_zone];
    }
}
