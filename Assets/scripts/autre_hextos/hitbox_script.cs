﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class hitbox_script : MonoBehaviour
{
    [SerializeField] Personnage personnage;
    List<Personnage> liste_victimes;
    // Start is called before the first frame update
    void Start()
    {
        liste_victimes = new List<Personnage>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        Personnage victime_potentielle;
        try
        {
            victime_potentielle = collision.gameObject.GetComponent<Personnage>();
        }
        catch
        {
            return;
        }
        foreach (Personnage victime in liste_victimes)
        {
            if (victime == victime_potentielle)
            {
                return;
            }
        }
        //maintenant on victimise
        victime_potentielle.subirDegats(Random.Range(personnage.puissanceMin, personnage.puissanceMax));
        //la victime potentielle devient une victime et ne peut plus être victimisé :))
        liste_victimes.Add(victime_potentielle);
    }
    public void Reset_list()
    {
        liste_victimes.Clear();
    }
}
