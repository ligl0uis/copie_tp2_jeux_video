using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Classe contenant toutes les fonctionnalités communes à tous
/// les personnages du jeu
/// </summary>
public class Personnage : MonoBehaviour
{
    [Header("Personnage")]
    // Points de vie
    [SerializeField] protected float pointsVieMax = 100f;
    [SerializeField] protected float pointsVie;
    [SerializeField] private GameObject barredevie_objet;
    protected barre_de_vie barre_De_Vie;

    // Déplacements
    [SerializeField] protected float vitesse;

    // Attaque
    // Dégats infligés par le personnage
    [SerializeField] public float puissanceMin;
    [SerializeField] public float puissanceMax;
    [SerializeField] protected float delaiAttaque;

    // Animations
    [SerializeField] protected bool estMort;

    // Audio
    [SerializeField] protected AudioClip[] acSubirDegats;
    [SerializeField] protected AudioClip[] acMourir;
    [SerializeField] protected AudioClip[] acAttaquer;
    [SerializeField] private GameObject prefab_degats;
    private IndicateurDegats indicateurDegats;
    protected Animator animator;
    AudioSource joueuraudio;
    protected virtual void Start()
    {
        indicateurDegats = GetComponent<IndicateurDegats>();
        animator = GetComponent<Animator>();
        joueuraudio = GetComponent<AudioSource>();
        barre_De_Vie = barredevie_objet.GetComponent<barre_de_vie>();
    }

    // Appelé lorsque le personnage subit des dégats
    public void subirDegats(float degats)
    {
        Debug.Log(this.name + " a subit "+ degats.ToString()+" degats");
        // Si je suis déjà mort, on ne fait rien
        if (estMort)
            return;

        // Calculer les pv restants
        pointsVie -= degats;
        barre_De_Vie.damage_barre(Mathf.RoundToInt((pointsVie * 100) / pointsVieMax));
        indicateurDegats.Setup(degats,this.transform,prefab_degats);
        // Vérifier si le personnage est mort
        if (pointsVie <= 0f)
        {
            pointsVie = 0f;
            mourir();
        }
        else
        { 
            animator.SetTrigger("hurt");
            jouerEffetSonore(acSubirDegats);
        }
    }

    public virtual void mourir()
    {
        estMort = true;
        animator.SetBool("dead", true);
        jouerEffetSonore(acMourir);
    }

    // Joue un effet sonore aléatoire d'une liste
    protected void jouerEffetSonore(AudioClip[] liste)
    {
        joueuraudio.PlayOneShot(liste[Random.Range(0, liste.Length)]);
    }

    public void soigner(float soin)
    {
        if ((pointsVie + soin) > pointsVieMax)
        {
            pointsVie = pointsVieMax;
        }
        else
        {
            pointsVie += soin;
        }
        barre_De_Vie.damage_barre(Mathf.RoundToInt((pointsVie * 100) / pointsVieMax));
    }
}
