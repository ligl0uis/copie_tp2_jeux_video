using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthEntity : MonoBehaviour
{
    public Animator animator;
    public Slider healthbar;

    const float santeMax = 100f;
    float sante;

    void Start()
    {
        sante = santeMax;

        // Paramétrer la healthbar
        healthbar.maxValue = santeMax;
        healthbar.value = santeMax;
    }

    public void subirDegats(float degats)
    {
        sante -= degats;

        healthbar.value = sante;

        // Vérifier si je suis mort!
        if (sante <= 0f)
        {
            Debug.Log("Je suis mort");
            animator.SetBool("isDead", true);
        }
        else
        {
            animator.SetTrigger("hurt");
        }
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        float forceImpact = collision.relativeVelocity.magnitude;

        Debug.Log($"Impact d'une force de {forceImpact}");
    }
}
