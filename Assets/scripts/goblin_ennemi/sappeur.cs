﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class sappeur : Ennemi
{
    [SerializeField] GameObject projectile_prefab;
    float delai_bombe = 0;
    [SerializeField] float vitesseProjectile;
    [SerializeField] float coefficientvitessedistance_projectile;
    Vector2 position_cible;

    protected override void Start()
    {
        base.Start();
    }
    protected override void Update()
    {
        delai_bombe -= Time.deltaTime;
        if (joueur!=null)
        {
            if (Vector2.Distance(this.transform.position, joueur.transform.position) > distanceAttaque)
            { 
                agent.destination=joueur.transform.position;
            }
            else
            {
                //fuir
                agent.destination = transform.position + (transform.position-joueur.transform.position);

                if(delai_bombe<=0)
                {
                    position_cible = joueur.transform.position;
                    attack();
                }
            }
        }
        //il faut faire le base apré pour ne pas faire l'overlap pour rien...
        base.Update();
    }

    //copié de joueur car je n'avait pas le goût de faire un ranged_attacker class même si c'est de la bonne étiquette...
    protected override void attack()
    {
        base.attack();

        delai_bombe = delaiAttaque;
        // Créer une copie du projectile
        GameObject projectile = Instantiate(projectile_prefab, transform.position, Quaternion.identity);
        script_projectile_explosif script_Projectile_ = projectile.GetComponent<script_projectile_explosif>();

        // Rigidbody du projectile
        Rigidbody2D rb = projectile.GetComponent<Rigidbody2D>();


        // Position visée
        Vector2 cible = position_cible;

        // on calcule la cible ici et le MRAAM se dirigera lui meme ;)
        script_Projectile_.main(cible, Mathf.RoundToInt(Random.Range(puissanceMin, puissanceMax)), vitesseProjectile, coefficientvitessedistance_projectile, distanceAttaque);

    }
}
