﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class goblin_melee : Ennemi
{
    [SerializeField] public BoxCollider2D hitbox_attaque;
    protected float delai_epee = 0;
    protected hitbox_script _hitbox_script;
    // Start is called before the first frame update
    protected override void Start()
    {

        _hitbox_script = hitbox_attaque.GetComponent<hitbox_script>();
        base.Start();
    }

    // Update is called once per frame
    protected override void Update()
    {
        if (delai_epee > 0)
        {
            delai_epee -= Time.deltaTime;
        }
        if (joueur != null)
        {
            agent.destination = joueur.transform.position;
            


        }

        //il faut faire le base apré pour ne pas faire l'overlap pour rien...
        base.Update();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (estMort)
        {
            return;
        }
        delai_epee -= Time.deltaTime;
        //jespere que la premiere condition passe en priorité de la deuxieme
        if (joueur != null && collision.gameObject==joueur)
        {
            if (delai_epee <= 0)
            {
                //la detection pour decider de l'animation est enervante, si j'avais plus de temps je pourrait probablement cuisiner qchose de plus intelligent
                float ydiff = this.transform.position.y - joueur.transform.position.y;
                switch (ydiff)
                {
                    //diff + = en bas , diff - = en haut
                    case var _ when ydiff>0.3f:
                        attack_melee("2");
                        break;
                    case var _ when ydiff < 0.3f:
                        attack_melee("1");
                        break;
                    default:
                        attack_melee("0");
                        break;
                }
                    
            }
        }
    }
    //enervant d'enlever l'override pour rajouter un parametre
    //j'ai rarement fait du code aussi spaghettifié
    void attack_melee(string id)
    {
        jouerEffetSonore(acAttaquer);
    
        delai_epee = delaiAttaque;

        //0=droite gauche 1= haut 2= bas
        animator.SetTrigger("attack"+id);
    
    }
}
