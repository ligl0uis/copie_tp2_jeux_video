using System.Collections;
using System.Collections.Generic;
using UnityEditor.Experimental.GraphView;
using UnityEngine;
using UnityEngine.AI;

public class Ennemi : Personnage
{
    GameManager gameManager;
    [Header("Ennemi")]
    // Vision
    [SerializeField] float distanceVision;
    [SerializeField] protected float distanceAttaque;
    [SerializeField] float laisse_distance;

    Collider2D[] overlaps;
    protected GameObject joueur;
    protected NavMeshAgent agent;
    Vector2 baseposition;
    bool lock_AI = false;
    AutomaticSpriteFlip flippeux;

    protected override void Start()
    {
        gameManager = FindFirstObjectByType<GameManager>();
        Debug.Log(gameManager.gameObject.name);
        base.Start();
        agent = GetComponent<NavMeshAgent>();
        baseposition = this.transform.position;
        flippeux = new AutomaticSpriteFlip(this.transform);
        pointsVie = pointsVieMax;
    }

    protected virtual void Update()
    {
        if (agent.velocity.magnitude != 0)
        {
            if (agent.velocity.x < 0f)
            {
                flippeux.SetDirection(AutomaticSpriteFlip.Direction.gauche);
            }
            else if (agent.velocity.x > 0f)
            {
                flippeux.SetDirection(AutomaticSpriteFlip.Direction.droite);
            }
            animator.SetBool("movement", true);
        }
        else
        {
            animator.SetBool("movement", false);
        }
        //si lock_AI le goblin DOIT revenir a sa position de depart avant de faire quoique ce soit
        if (lock_AI)
        {
            if(Vector2.Distance(this.transform.position,baseposition)<0.3f)
            {
                Debug.Log("goblin unlock");
                lock_AI = false;
            }
            return;
        }

        if (joueur != null)
        {
            //Debug.Log(agent.destination.ToString());
            if(Vector2.Distance(this.transform.position,baseposition)>laisse_distance)
            {
                Debug.Log("le goblin c'est lock");
                lock_AI = true;
                joueur = null;
                agent.destination = baseposition;
                return; 
            }
            if (Vector2.Distance(this.transform.position, joueur.transform.position) > distanceVision)
            {
                Debug.Log("le joueur est sorti de la vision");
                joueur = null;
                return;
            }
            else
            {
                agent.destination = baseposition;
                //pour ne pas faire la verification d'overlap si le joueur est en portée
                return;
            }
        }
        //essentielment un radar qui essai de trouver un gameobject avec joueur qui est ensuite garder en mémoire jusquace qu'il sort de la vision ou que le goblin sort de la laisse/leash
        overlaps = Physics2D.OverlapCircleAll(transform.position, distanceVision);
        foreach (Collider2D overlap in overlaps)
        {
            //on en apprend tout les jours! les discards sont utiles!
            if (overlap.gameObject.TryGetComponent<Joueur>(out _))
            {
                Debug.Log("joueur détecté!");
                joueur = overlap.gameObject;
                break;
            }
        }
       
    }

    public override void mourir()
    {
        base.mourir();
        gameManager.ennemy_check(this.transform.position);
        agent.velocity = Vector2.zero;
        agent.isStopped = true;


        // TODO Apparition de prefab du crâne
        // fait a l'aide de l'animator, pas besoin d'un prefab juste pour une animation

    }
    protected virtual void attack()
    {
        if(estMort)
        {
            return;
        }
        jouerEffetSonore(acAttaquer);
        animator.SetTrigger("attack");
    }
}
