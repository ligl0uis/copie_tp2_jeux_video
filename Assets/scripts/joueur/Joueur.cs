using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Joueur : melee_attacker
{


    [Header("Joueur")]

    float horizontal, vertical;

    Vector2 direction;

    Vector3 defaultScale;
    Vector3 tempScale;

    Rigidbody2D rb;
    [SerializeField] GameObject projectile_prefab;

    [SerializeField] Toki Toki;

    [SerializeField] float distanceMax;

    [SerializeField] float vitesseProjectile;
    [SerializeField] float coefficientvitessedistance_projectile;
    [SerializeField] AudioClip[] sfx_epee;


    float delai_bombe = 0;


    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();
        defaultScale = transform.localScale;
        tempScale = defaultScale;
        rb = GetComponent<Rigidbody2D>();
        pointsVie = pointsVieMax;
    }


    public override void mourir()
    {
        base.mourir();
        // Appeler la résurection
        Invoke("ressusciter", 3f);
        GameObject.FindObjectOfType<GameManager>().changer_zone(0);
    }

    void ressusciter()
    {
        // TODO Téléporter le joueur au village
        transform.position = new Vector2(0, 0);
        // Remettre sa vie à pleine capacité
        pointsVie = pointsVieMax;
        barre_De_Vie.damage_barre(Mathf.RoundToInt(pointsVie));
        // Retirer le statut de estMort
        estMort = false;
        animator.SetBool("dead", false);

        // TODO Aviser Toki de ma résurrection
        Toki.joueurEstReapparu();
    }

    void Update()
    {
        if (!estMort)
        {
            // Assigner les valeurs d'input d'axes
            horizontal = Input.GetAxisRaw("Horizontal");
            vertical = Input.GetAxisRaw("Vertical");

            direction.x = horizontal;
            direction.y = vertical;

            // Normaliser le vecteur de déplacement pour ne pas accéler lorsque
            // je me déplace en angle de 45 degrés
            direction.Normalize();

            // Vérifier la direction du mouvement
            if (direction.magnitude != 0)
            {
                animator.SetBool("movement", true);
                if (horizontal < 0f)
                {
                    tempScale.x = defaultScale.x * -1f;
                }
                else if (horizontal > 0f)
                {
                    tempScale.x = defaultScale.x;
                }
            }
            else
            {
                animator.SetBool("movement", false);
            }
            // Assigner le scale
            transform.localScale = tempScale;

            if (Input.GetButtonDown("Fire2") && delai_bombe <= 0)
            {
                attaque_bombe();
            }
            if (Input.GetButtonDown("Fire1") && delai_epee <= 0)
            {
                attaque_épée(sfx_epee);
            }
        }
    }

    void FixedUpdate()
    {
        // Déplacement du personnage

        rb.velocity = direction * vitesse * Time.fixedDeltaTime;
        if(delai_bombe>=0)
        {
            delai_bombe -= Time.fixedDeltaTime;
        }
        if(delai_epee>=0)
        {
            delai_epee -= Time.fixedDeltaTime;
        }

    }




    void attaque_bombe()
    {
        jouerEffetSonore(acAttaquer);
        Vector2 position_cible = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        delai_bombe = delaiAttaque * 2;
        // Créer une copie du projectile
        GameObject projectile = Instantiate(projectile_prefab, transform.position, Quaternion.identity);
        script_projectile_explosif script_Projectile_ = projectile.GetComponent<script_projectile_explosif>();

        // Rigidbody du projectile
        Rigidbody2D rb = projectile.GetComponent<Rigidbody2D>();


        // Position visée
        Vector2 cible = position_cible;

        // on calcule la cible ici et le MRAAM se dirigera lui meme ;)
        script_Projectile_.main(cible,Mathf.RoundToInt(Random.Range(puissanceMin,puissanceMax)),vitesseProjectile,coefficientvitessedistance_projectile,distanceMax);

    }
}

    

