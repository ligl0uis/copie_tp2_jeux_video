using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UIElements;

public class barre_de_vie_joueur : barre_de_vie
{

    private TextMeshProUGUI text;

    protected override void Start()
    {
        base.Start();
        text = transform.Find("txtPointsVie").GetComponent<TextMeshProUGUI>();
    }
    public override void damage_barre(int vie)
    {
        base.damage_barre(vie);
        text.text = vie.ToString(); 
    }
}
