﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//considerant que unity ne permet permet l'héritance multiple j'aurait du faire une classe a part et l'ajouter en dépendance au joueur et au goblin...
//le goblin va tout simplement réutiliser du code de cette classe...
public class melee_attacker : Personnage
{
    [SerializeField] public BoxCollider2D hitbox_attaque;
    protected float delai_epee = 0;
    protected hitbox_script _hitbox_script;

    protected override void Start()
    {
       base.Start();
        _hitbox_script = hitbox_attaque.GetComponent<hitbox_script>();
    }
    protected void attaque_épée(AudioClip[] sfx)
    {
        delai_epee = delaiAttaque;

        //un peut batard a faire mais quand on attaque il faut reset la liste de victimes qui est dans un script dans la hitbox
        _hitbox_script.Reset_list();
        jouerEffetSonore(sfx);

        animator.SetTrigger("attack");
    }
}
