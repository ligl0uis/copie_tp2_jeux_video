using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class ScriptPourCorrigerLaDistanceMaximaleDeTirJespereQueVousChangerezLeNom_nonjerefusedechangerlenom : MonoBehaviour
{
    public Transform debugPosition;

    float distanceMax = 3f;

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            corrigerPositionLance();
        }        
    }

    void corrigerPositionLance()
    {
        // La position du personnage
        Vector2 position = transform.position;

        // Position du curseur dans le World
        Vector2 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        // Position visée
        Vector2 cible = mousePosition;

        // Distance entre le personnage et sa cible
        float distance = Vector2.Distance(position, cible);

        // Si la cible est plus loin que la distance maximale de tir, on corrigera la position de la cible
        if (distance > distanceMax)
        {
            Vector2 direction = (mousePosition - position).normalized;

            // Cible corrigée
            cible = position + direction * distanceMax;
        }
        // Utilisation de la cible
        debugPosition.position = cible;
    }
}
